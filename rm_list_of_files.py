#!/usr/bin/python3
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("list_of_files", type=str, help="Absolute Path to file")
list_of_files = parser.parse_args().list_of_files

count_deleted = 0
count_error = 0

with open(list_of_files) as list_of_files:
    filelist = list_of_files.readlines()
    for file in filelist:
        try:
            Path(file.strip()).unlink()
            count_deleted += 1
        except:
            print("{0} not found".format(file.strip()))
            count_error += 1
    print(
        "Files deleted: {0}, Filepaths with error: {1}".format(
            count_deleted, count_error
        )
    )
